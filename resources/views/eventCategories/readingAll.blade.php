<aside>
    <table>
        <thead>
            <tr>
                <td>select</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($eventCategoryList as $eventCategory) 
            <tr>
                <td><a href="{{ route('eventCategories.show',$eventCategory->id)}}">show</a></td>
                <td>{{$eventCategory->name}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</aside>