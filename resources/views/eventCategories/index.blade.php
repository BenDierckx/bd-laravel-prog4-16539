<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="Ben Dierckx">
        <link rel="stylesheet" href="{{ asset('css/fricfrac.css') }}" type="text/css"/>
        <title>Fric-Frac</title>
    </head>
    <body>
        <main>
            <article>
                <header>
                    <h2>EventCategory</h2>
                        <nav>
                            <a href="{{route('eventCategories.create')}}">Create</a>
                        </nav>
                </header>
                <nav>
                    <fieldset>
                        <div>
                        </div>
                    </fieldset>
                </nav>
            </article>
            @include('eventCategories.readingAll')
        </main>
    </body>
</html>