<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Ben Dierckx">
    <link rel="stylesheet" href="{{ asset('css/fricfrac.css') }}" type="text/css"/>
    <title>Fric-Frac</title>
  </head>
  <body>
    <main>
      <article>
        <header>
    			<h2>EventCategory</h2>
    			<nav>
    				<a href="{{route('eventCategories.edit', ['id' => $eventCategory->id])}}">Edit</a>
    				<a href="{{route('eventCategories.create')}}">Create</a>
    				<button type="submit" value="delete" form="form">Delete</button>
    				<a href="{{route('eventCategories.index')}}">Annuleren</a>
    			</nav>
    		</header>
        <form action="{{ route('eventCategories.destroy', ['id' => $eventCategory->id])}}" id="form" method="post">
          @csrf
          @method('DELETE')
    			<div>
    				<label for="Name">Naam</label>
    				<input type="text" readonly id="Name" name="name" value="{{$eventCategory->name}}">
    			</div>
        </form>
        <a href={{route('eventCategories.index')}}>index</a>
      </article>
    </main>
  </body>
</html>

