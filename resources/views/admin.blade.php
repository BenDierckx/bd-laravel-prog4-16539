<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Ben Dierckx">
    <link rel="stylesheet" href="{{ secure_asset('css/fricfrac.css') }}" type="text/css"/>
    <title>Beheer Fric-Frac</title>
</head>
<body>
    <header>
        <h1>Fric-Frac</h1>
    </header>
    <main>
        <section>
            <!-- a.tile[id]*11 -->
            <a href="Person/Index.php" class="tile" id=""><span>Persoon</span></a>
            <a href="{{route('countries.index')}}" class="tile" id=""><span>Land</span></a>
            <a class="tile" id=""></a>
            <a class="tile" id=""></a>
            <a href="Role/Index.php" class="tile" id=""><span>Role</span></a>
            <a href="User/Index.php" class="tile" id=""><span>Gebruiker</span></a>
            <a class="tile" id=""></a>
            <a href="Event/Index.php" class="tile" id=""><span>Event</span></a>
            <a href="{{route('eventCategories.index')}}" class="tile" id=""><span>Event Categorie</span></a>
            <a href="EventTopic/Index.php" class="tile" id=""><span>Event Topic</span></a>
            <a class="tile" id=""></a>
        </section>
    </main>
    <footer>
        <p>CVO Antwerpen 2018-2019 Fric-Frac</p>
    </footer>
</body>
</html>