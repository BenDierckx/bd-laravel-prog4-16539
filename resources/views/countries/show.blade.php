<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Ben Dierckx">
    <link rel="stylesheet" href="{{ asset('css/fricfrac.css') }}" type="text/css"/>
    <title>Fric-Frac</title>
  </head>
  <body>
    <main>
      <article>
        <header>
    			<h2>Land</h2>
    			<nav>
    				<a href="{{route('countries.edit', ['id' => $country->id])}}">Edit</a>
    				<a href="{{route('countries.create')}}">Create</a>
    				<button type="submit" value="delete" form="form">Delete</button>
    				<a href="{{route('countries.index')}}">Annuleren</a>
    			</nav>
    		</header>
        <form action="{{ route('countries.destroy', ['id' => $country->id])}}" id="form" method="post">
          @csrf
          @method('DELETE')
    			<div>
    				<label for="Name">Naam</label>
    				<input type="text" readonly id="Name" name="name" value="{{$country->name}}">
    			</div>
    			<div>
    				<label for="Code">Code</label>
    				<input type="text" readonly id="Code" name="countryCode" value="{{$country->code}}">
    			</div>
        </form>
        <a href={{route('countries.index')}}>index</a>
      </article>
    </main>
  </body>
</html>

