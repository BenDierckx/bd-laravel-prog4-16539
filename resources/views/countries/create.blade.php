<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Ben Dierckx">
    <link rel="stylesheet" href="{{ asset('css/fricfrac.css') }}" type="text/css"/>
    <title>Fric-Frac</title>
  </head>
  <body>
    <main>
      <article>
        <header>
          <h2>Land</h2>
          <nav>
            <a href="{{route('countries.index')}}">Annuleren</a>
          </nav>
        </header>
        <form method="post" action="{{ route('countries.store') }}">
          <div class="form-group">
            @csrf
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">
            <label for="code">code :</label>
            <input type="text" class="form-control" name="code"/>
          </div>
          <button type="submit" class="btn btn-primary">Add</button>
        </form>
      </article>
      @include('countries.readingAll')
  </main>
</body>
</html>