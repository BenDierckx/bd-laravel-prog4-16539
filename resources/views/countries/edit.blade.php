<?php //edit = updateone ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Ben Dierckx">
    <link rel="stylesheet" href="{{ asset('css/fricfrac.css') }}" type="text/css"/>
    <title>Fric-Frac</title>
  </head>
  <body>
    <main>
        <article>
          <header>
              <h2>Land</h2>
          <nav>
             <a href="{{route('countries.index')}}">Annuleren</a>
          </nav>
          </header>
          <form method="post" action="{{ route('countries.update', $country->id) }}">
              @method('PATCH')
              @csrf
              <div>
                <label for="name">Name:</label>
                <input type="text"  name="name" value="{{ $country->name }}" />
              </div>
              <div>
                <label for="code">code :</label>
                <input type="text" name="code" value="{{ $country->code }}" />
              </div>
              
              <button type="submit">Update</button>
          </form>
        </article>
        @include('countries.readingAll')
    </main>
  </body>
</html>