<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EventCategory;

class EventCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventCategories = EventCategory::all();
        return view('eventCategories.index', array('eventCategoryList' =>$eventCategories));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eventCategories = EventCategory::all();
        return view('eventCategories.create', array('eventCategoryList' =>$eventCategories));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
        ]);
        $eventCategory = new EventCategory;
        $eventCategory->name = $request->name;
        $eventCategory->save();
        return redirect('/eventCategories')->with('success', 'eventCategory has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eventCategory = EventCategory::find($id);
        $eventCategoryList = EventCategory::all();
        
        return view('eventCategories.show', array('eventCategory' => $eventCategory));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventCategory = EventCategory::find($id);
        $eventCategoryList = EventCategory::all();
        return view('eventCategories.edit', compact('eventCategory', 'eventCategoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);

        $eventCategory = EventCategory::find($id);
        $eventCategory->name = $request->get('name');
     
        $eventCategory->save();

        return redirect('eventCategories')->with('success', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventCategory = EventCategory::find($id);
        $eventCategory->delete();
        
        return redirect('eventCategories');
    }
}
